erlang-proper (1.2+git988ea0ed9f+dfsg-2) unstable; urgency=medium

  * Change maintainer address to Debian Erlang Packagers
    <pkg-erlang-devel@lists.alioth.debian.org>. (Closes: #899823)
  * Bump Standards-Version to 4.1.4.
  * Change Vcs-Browser and Vcs-Git to salsa.
  * Update debhelper to 11.
  * Update debian/copyright.
    - Add Upstream-Name and Upstream-Contact.
    - Update Format field in copyright.

 -- Nobuhiro Iwamatsu <iwamatsu@debian.org>  Fri, 01 Jun 2018 07:43:12 +0900

erlang-proper (1.2+git988ea0ed9f+dfsg-1) unstable; urgency=medium

  * Snapshot, taken from the master (20170823). (Closes: #868659)
  * Update debian/patches/fix_build_on_kfreebsd.patch.
  * Update Standards-Version to 4.1.0.

 -- Nobuhiro Iwamatsu <iwamatsu@debian.org>  Wed, 23 Aug 2017 12:18:11 +0900

erlang-proper (1.2+dfsg-1) unstable; urgency=medium

  * Update to 1.2.
  * Add debian/gbp.conf.
  * Update debian/control.
    Update Standards-Version to 3.9.8.
  * Fix build on kFreeBSD. (Closes: #815403)
    Add patches/fix_build_on_kfreebsd.patch.

 -- Nobuhiro Iwamatsu <iwamatsu@debian.org>  Wed, 07 Sep 2016 01:30:38 +0900

erlang-proper (1.1+gitfa58f82bdc+dfsg-1) unstable; urgency=medium

  * Snapshot, taken from the master (20150725). (Closes #790444)
  * Update debian/control.
    Update Standards-Version to 3.9.6.

 -- Nobuhiro Iwamatsu <iwamatsu@debian.org>  Sat, 25 Jul 2015 01:25:52 +0900

erlang-proper (1.1+dfsg-1) unstable; urgency=low

  * Update to 1.1.
  * Remove patches/0001-Update-Makefile.-Change-to-use-rebar-of-system.patch.

 -- Nobuhiro Iwamatsu <iwamatsu@debian.org>  Fri, 11 Oct 2013 08:42:44 +0900

erlang-proper (1.0+20120524+dfsg-5) unstable; urgency=low

  * Update debian/control.
    - Add dh-rebar to Build-Depends.
  * Update debian/rules.
    - Changed to use dh-rebar.
  * Add debian/watch.

 -- Nobuhiro Iwamatsu <iwamatsu@debian.org>  Mon, 19 Aug 2013 21:22:40 +0900

erlang-proper (1.0+20120524+dfsg-3) unstable; urgency=low

  * Fix FTBFS when not builing -doc package. (Closes: #716980)

 -- Nobuhiro Iwamatsu <iwamatsu@debian.org>  Tue, 16 Jul 2013 04:42:30 +0900

erlang-proper (1.0+20120524+dfsg-2) unstable; urgency=low

  * Update debian/control.
    - Add Vcs-Git field and Vcs-Browser field.
    - Add erlang-edoc to Build-Depends.
    - Add erlang-proper-doc package.
  * Add edoc to erlang-proper-dev. (Closes: #701813)
  * Update debian/rules.
    - Build edoc.
    - Change call "rebar clean" instead of make clean.
    - Remove verbose option from from erlang-depends.

 -- Nobuhiro Iwamatsu <iwamatsu@debian.org>  Thu, 04 Jul 2013 12:53:36 +0900

erlang-proper (1.0+20120524+dfsg-1) unstable; urgency=low

  * Initial release. (Closes: #689388)

 -- Nobuhiro Iwamatsu <iwamatsu@debian.org>  Thu, 02 Aug 2012 12:38:43 +0900
