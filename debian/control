Source: erlang-proper
Maintainer: Debian Erlang Packagers <pkg-erlang-devel@lists.alioth.debian.org>
Uploaders: Taku YASUI <tach@debian.org>, Nobuhiro Iwamatsu <iwamatsu@debian.org>
Section: devel
Priority: optional
Standards-Version: 4.1.4
Homepage: https://github.com/manopapad/proper
Vcs-Browser: https://salsa.debian.org/erlang-team/packages/erlang-proper
Vcs-Git: https://salsa.debian.org/erlang-team/packages/erlang-proper.git
Build-Depends: debhelper (>= 11), dpkg-dev (>= 1.16.1~), erlang-dev (>= 1:13.b.4),
	erlang-eunit, erlang-edoc, dh-rebar

Package: erlang-proper
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends},
	erlang-base-hipe | erlang-base | ${erlang-abi:Depends}, ${erlang:Depends}
Description: QuickCheck-inspired property-based testing tool for Erlang
 PropEr (PROPerty-based testing tool for ERlang) is a QuickCheck-inspired
 open-source property-based testing tool for Erlang.

Package: erlang-proper-dev
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}, erlang-proper (>= ${source:Version})
Description: QuickCheck-inspired property-based testing tool for Erlang - development files
 PropEr (PROPerty-based testing tool for ERlang) is a QuickCheck-inspired
 open-source property-based testing tool for Erlang.
 .
 This package includes erlang-proper headers which are necessary to build Erlang
 applications which use erlang-proper.

Package: erlang-proper-doc
Architecture: all
Section: doc
Suggests: erlang-proper-dev
Depends: ${misc:Depends}
Multi-Arch: foreign
Description: QuickCheck-inspired property-based testing tool for Erlang - document files
 PropEr (PROPerty-based testing tool for ERlang) is a QuickCheck-inspired
 open-source property-based testing tool for Erlang.
 .
 This package includes erlang-proper documents.
